from selenium import webdriver
import pickle
import os

email = "hello@daruma.co.id"
password = "Hellodaruma123"


def authetication(headless=False):
    is_exists = os.path.isfile("cookies-%s.pkl" % email)
    options = webdriver.FirefoxOptions()
    # options.add_argument('-headless')
    if headless:
        options.add_argument('-headless')
    driver = webdriver.Firefox(firefox_options=options, executable_path=os.path.join(os.getcwd(), "geckodriver"))
    if is_exists:
        driver.set_page_load_timeout(120)
        driver.get("https://tokopedia.com/login")
        cookies = pickle.load(open("cookies-%s.pkl" % email, "rb"))
        for cookie in cookies:
            driver.add_cookie(cookie)
        driver.get("https://tokopedia.com/login")
        print("logged in with cookies..")
        return driver
    else:
        driver.set_page_load_timeout(120)
        print("cookies not found..")
        driver.get("https://tokopedia.com/login")
        email_field = driver.find_element_by_name("email")
        password_field = driver.find_element_by_name("password")
        email_field.send_keys(email)
        driver.implicitly_wait(1)
        password_field.send_keys(password)
        driver.implicitly_wait(1)
        driver.find_element_by_class_name("js__submit-login").click()
        pickle.dump(driver.get_cookies(), open("cookies-%s.pkl" % email, "wb"))
        print("Logged in..")
        return driver

from selenium import webdriver
import os
import _constants
import time
import pickle
import _constants
from authentication import authetication
import requests
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


def update_stok(item_name, description=None, price=None, stok=None, headless=False, store_url=None, new_name=None):
    print("updating %s, please wait.." % item_name)

    email = os.environ['TOKOPEDIA_USERNAME']
    password = os.environ['TOKOPEDIA_PASSWORD']

    if store_url and str(store_url)[-1] != "/":
        store_url = str(store_url) + "/"

    options = webdriver.FirefoxOptions()
    if headless:
        options.add_argument('-headless')
    driver = webdriver.Firefox(firefox_options=options, executable_path=os.path.join(os.getcwd(), "geckodriver"))

    def click_component(i):
        try:
            i.click()
        except:
            return False

    # login
    driver.get("https://www.tokopedia.com/login")
    email_field = driver.find_element_by_name("email")
    password_field = driver.find_element_by_name("password")

    email_field.send_keys(email)
    driver.implicitly_wait(1)

    password_field.send_keys(password)
    driver.implicitly_wait(1)

    driver.find_element_by_class_name("js__submit-login").click()

    home = str(driver.current_url)
    if home != "https://www.tokopedia.com/":
        print("login failed")
        if headless:
            driver.quit()
        return
    else:
        print("logged in..")

    name = str(item_name.strip()).lower().replace(" ", "-").replace("%", "").replace("(", "").replace(")", "")
    if store_url:
        try:
            driver.get("%s/%s" % (store_url, name))
            driver.find_element_by_class_name("rvm-button-seller").click()
        except:
            print("updating failed: item not found..")
            if headless:
                driver.quit()
            return
    else:
        try:
            driver.get(driver.current_url)
            store_url = driver.find_element_by_css_selector(".box-media__content a").get_attribute("href")
            store_url = store_url[:store_url.find("?")]
            driver.get("%s/%s" % (store_url, name))
            driver.find_element_by_class_name("rvm-button-seller").click()
        except:
            print("updating failed: item not found..")
            if headless:
                driver.quit()
            return

    # updating name field
    if new_name:
        print("updating name..")
        try:
            driver.find_element_by_id("p-name").clear()
            name_field = driver.find_element_by_id("p-name")
            name_field.send_keys(new_name)
            driver.implicitly_wait(1)
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        except:
            print("updating name failed, name field can not be changed..")

    # updating description field
    if description:
        print("updating description..")
        driver.find_element_by_id("p-description").clear()
        description_field = driver.find_element_by_id("p-description")
        description_field.send_keys(description)
        driver.implicitly_wait(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    else:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # updating price field
    if price:
        print("updating price..")
        driver.find_element_by_id("p-price").clear()
        price_field = driver.find_element_by_id("p-price")
        price_field.send_keys(price)
        driver.implicitly_wait(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    else:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # updating stok field
    if stok:
        print("updating stok..")
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        element = driver.find_element_by_css_selector("input[type='radio'][value='limited_stock']")
        while click_component(element) == False:
            print("re-click..")
            click_component(element)
        driver.implicitly_wait(1)
        driver.find_element_by_id("invenage-value").clear()
        stock_field = driver.find_element_by_id("invenage-value")
        stock_field.send_keys(stok)
        driver.implicitly_wait(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # save
    save_product = driver.find_element_by_name("submit")
    while click_component(save_product) == False:
        click_component(save_product)

    print("%s has been updated.." % item_name)

    if headless:
        driver.quit()
    return


def download_pic(image_url):
    try:
        with open("pictures/product.jpg", 'wb') as handle:
            response = requests.get(image_url, stream=True)

            if not response.ok:
                print(response)

            for block in response.iter_content(1024):
                if not block:
                    break
                handle.write(block)
            print("pic downloaded")
            return True
    except:
        return False


def update_stok_by_tokped_id(driver, tokped_id, image_url=None, description=None, price=None, stock=None, new_name=None,
                             headless=False):
    print("updating %s, please wait.." % tokped_id)

    def click_component(i):
        try:
            i.click()
        except:
            return False

    driver.get("https://www.tokopedia.com/product-edit.pl?id=%s" % tokped_id)
    try:
        js = "window.onbeforeunload = function(e){};"
        driver.execute_script(js)
    except Exception as e:
        print(str(e))
    if image_url:
        print("updating image..")
        while not download_pic(image_url):
            print("image not downloaded, retrying..")
            download_pic(image_url)

        def click_remove_pic():
            try:
                remove_button = driver.find_element_by_class_name("picture-action--delete")
                remove_button.click()
            except:
                return False

        trying = 0
        while not click_remove_pic():
            if trying == 3:
                break
            print("reclicking remove pic..")
            click_remove_pic()
            trying += 1
        driver.implicitly_wait(1)

        image_path = os.path.join(os.getcwd(), "pictures", "product.jpg")
        js = "document.getElementById('ngf-pickfiles-nav1').style.visibility = 'visible';"
        driver.execute_script(js)
        field = driver.find_element_by_id("ngf-pickfiles-nav1")
        field.send_keys(image_path)
        driver.implicitly_wait(1)
        time.sleep(5)

        # def click_pop_up():
        #     try:
        #         close = driver.find_element_by_class_name("unf-modal__close")
        #         close.click()
        #     except Exception as e:
        #         print(str(e))
        #         return False
        #
        # trying = 0
        # while not click_pop_up():
        #     if trying == 3:
        #         break
        #     print("reclicking pop up..")
        #     click_pop_up()
        #     trying += 1

        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # updating name field
    if new_name:
        print("updating name..")
        try:
            driver.find_element_by_id("p-name").clear()
            name_field = driver.find_element_by_id("p-name")
            name_field.send_keys(new_name)
            driver.implicitly_wait(1)
            time.sleep(1)
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        except:
            print("updating name failed, name field can not be changed..")

    # updating description field
    if description:
        print("updating description..")
        driver.find_element_by_id("p-description").clear()
        description_field = driver.find_element_by_id("p-description")
        description_field.send_keys(description)
        driver.implicitly_wait(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    else:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # updating price field
    if price:
        print("updating price..")
        driver.find_element_by_id("p-price").clear()
        price_field = driver.find_element_by_id("p-price")
        price_field.send_keys(price)
        driver.implicitly_wait(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    else:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # updating stok field
    if stock:
        print("updating stok..")
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        element = driver.find_element_by_css_selector("input[type='radio'][value='limited_stock']")
        while click_component(element) == False:
            print("re-click..")
            click_component(element)
        driver.implicitly_wait(1)
        driver.find_element_by_id("invenage-value").clear()
        stock_field = driver.find_element_by_id("invenage-value")
        stock_field.send_keys(stock)
        driver.implicitly_wait(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # save
    print("clicking save..")
    # save_product = driver.find_element_by_class_name("btn-action")
    save_product = driver.find_element_by_name("submit")
    trying = 0
    while click_component(save_product) == False:
        if trying > 3:
            break
        print("reclicking..")
        click_component(save_product)
        trying += 1

    try:
        mySelectElement = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.CLASS_NAME, "unf-modal__close")))
        mySelectElement.click()
    except:
        pass

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(5)
    current_url = str(driver.current_url)
    while current_url != "https://www.tokopedia.com/manage-product-new.pl":
        print("clicking save..")
        save_product = driver.find_element_by_name("submit")
        trying = 0
        while click_component(save_product) == False:
            if trying > 3:
                break
            print("reclicking..")
            click_component(save_product)
            trying += 1

        try:
            mySelectElement = WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.CLASS_NAME, "unf-modal__close")))
            mySelectElement.click()
        except:
            pass
    time.sleep(5)
    print("product was updated..")


if __name__ == "__main__":
    image_url = "https://vignette.wikia.nocookie.net/mob-psycho-100/images/8/8c/Mob_anime.png/revision/latest?cb=20160712054631"
    # update_stok_by_tokped_id(tokped_id="396441436", new_name="Tes Update Nama Baru")

import csv
import pprint

def read_tsv(filename):
    result = list()
    with open(filename) as fd:
        rd = csv.reader(fd, delimiter="\t", quotechar='"')
        for row in rd:
            result.append(
                dict(
                    id=row[0],
                    name=row[1],
                    description=str(row[2]).replace("\n", ""),
                    description_tokopedia=str(row[3]).replace("\n", ""),
                    weight=row[4],
                    price=row[5],
                    min_order_qty=row[6],
                    availability=row[7],
                    inventory_qty=row[8],
                    condition=row[9],
                    gtin=row[10],
                    mpn=row[11],
                    brand=row[12],
                    image_1=row[13],
                    image_2=row[14]
                )
            )
    return result[1:]


def read_csv(filename):
    result = list()
    with open(filename, encoding="utf-8") as fd:
        rd = csv.reader(fd, delimiter=",", quotechar='"')
        for row in rd:
            try:
                cat = row[0].split("-")[2].strip()
            except:
                cat = row[0]
            result.append(
                dict(
                    category=cat.replace("Oben", "Obeng") if cat == 'Oben' else cat,
                    etalase=row[1],
                    id=row[2],
                    name=row[3],
                    description=row[4],
                    description_tokopedia=row[5],
                    weight=row[6],
                    price=row[7],
                    min_order_qty=row[8],
                    availability=row[9],
                    inventory_qty=row[10],
                    condition=row[11],
                    gtin=row[12],
                    mpn=row[13],
                    brand=row[14],
                    image_1=row[15],
                    image_2=row[16]
                )
            )
    return result[1:]

def read_csv_prod(filename):
    result = list()
    with open(filename) as fd:
        rd = csv.reader(fd, delimiter=",", quotechar='"')
        for row in rd:
            result.append(
                dict(
                    name=row[0],
                    image_url=row[1],
                    price=row[2],
                    stock=row[3],
                    description=row[4],
                    sku=row[5],
                    weight=row[6],
                    category=row[7],
                    etalase=row[8],
                    tokped_id=row[9],
                )
            )
    return result[1:]




#
prod = read_csv("products2.csv")

# print(len([i for i in prod if 'Bosch Velcro Sanding Disc 125mm P60 / G60' in i['name']]))
print(type(prod))
#
# for i in prod:
#     print(i['etalase'])

# category = [i.get('category').split('-')[2].strip() for i in read_csv("products.csv")]
# etalase = [i.get('etalase') for i in read_csv("products.csv")]
# print(set(category))
# print(read_csv("products.csv")[0])

# print(set(etalase))
# x = read_csv("products2.csv")
# for i in x:
#     print(i['category'])
# print(read_csv("products.csv"))
# pprint.pprint(read_csv("products.csv")[0:3])
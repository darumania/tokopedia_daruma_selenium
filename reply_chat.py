from authentication import authetication
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from mongo import insert_chat, update_chat, find_chat_by_name
from datetime import datetime


def reply_chat(sender_url, chat, headless: bool = False):

    driver = authetication(headless=headless)

    driver.get("https://www.tokopedia.com/chat")

    def get_list_chat_name():
        try:
            return driver.find_elements_by_class_name("list-chat-name")
        except:
            return None

    last_chat = list()
    while True:
        chat_list = driver.find_element_by_class_name("customScrollBar--list-chat")
        for scroll in range(6):
            chat_list.send_keys(Keys.PAGE_DOWN)
        top = driver.find_elements_by_class_name("list-chat-item-chat-name__container")[-1].text
        last_chat.append(top)
        if last_chat.count(top) > 2:
            break

    chat_names = get_list_chat_name()

    for i in chat_names:
        i.click()
        sender_url_target = driver.find_element_by_css_selector(".chat-header__profile .flex-center a").get_attribute("href")
        sender_name = driver.find_element_by_class_name("chat-header__profile-name").text
        if str(sender_url_target) == sender_url:
            print("sending chat..")
            mySelectElement = WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.CLASS_NAME, "emojionearea-editor")))
            mySelectElement.send_keys(chat)
            driver.implicitly_wait(1)
            driver.find_element_by_class_name("btn-submit-message").click()
            print("sent..")
            break


if __name__ == "__main__":
    reply_chat("https://www.tokopedia.com/people/39163616", chat="selamat datang di Daruma, jangan lupa maem :)", headless=True)

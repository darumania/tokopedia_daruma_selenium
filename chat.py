from authentication import authetication
import time
from mongo import insert_chat, update_chat, find_chat_by_name
from datetime import datetime


driver = authetication(headless=True)

def stream_chat():

    while True:

        driver.get("https://www.tokopedia.com/chat")

        def get_new_messages():
            try:
                return driver.find_elements_by_class_name("notif-wrap")
            except:
                return None

        new_messages = get_new_messages()

        if new_messages:
            for message in new_messages:
                message.click()
                chats = list()
                chat = driver.find_elements_by_class_name("chat-bubble__text-msg")
                for c in chat:
                    chats.append(c.text)
                sender_name = driver.find_element_by_class_name("chat-header__profile-name").text
                sender_url = driver.find_element_by_css_selector("div.chat-header__profile div.flex-center a").get_attribute("href")
                doc = dict(
                    sender_name=sender_name,
                    sender_url=sender_url,
                    chat=chats,
                    status="new messages",
                    created_at=datetime.now()
                )
                chatsender = find_chat_by_name(sender_url=sender_url)
                if chatsender:
                    update_chat(newchat=chats, sender_url=sender_url)
                    print("chat updated from %s" % sender_name)
                else:
                    insert = insert_chat(doc)
                    if insert:
                        print("chat inserted")
                    else:
                        print("chat not inserted")
                print("%s: %s" % (sender_name, chats))
        else:
            print("no new message")
        time.sleep(2)


if __name__ == "__main__":
    stream_chat()

import pymongo
import _constants

myclient = pymongo.MongoClient("mongodb://46.101.77.153:27113/")

productsdb = myclient['productsdb']

# daruma tokopedia products collection
if _constants.TESTING:
    collection = productsdb['products_dummy']
else:
    collection = productsdb['products']

# daruma tokopedia chat collection
collection_chat = productsdb['chat']

def insert_chat(doc):
    return collection_chat.insert_one(doc).acknowledged

def update_chat(newchat, sender_url):
    query = {"sender_url":sender_url}
    new_values = {
        "$set" : {
            "chat" : newchat
        }
    }
    return collection_chat.update_one(query, new_values).acknowledged

def find_chat_by_name(sender_url):
    result = collection_chat.find({"sender_url":sender_url})
    for i in result:
        return i

def insert_doc(doc):
    return collection.insert_one(doc).acknowledged

def find_by_sku(sku):
    result = collection.find({"sku":sku})
    for i in result:
        return i

def update_doc(new_doc, old_doc):
    query = old_doc
    new_values = {
        "$set" : new_doc
    }

    return collection.update_one(query, new_values).acknowledged

def delete_doc_by_sku(sku):
    query = {"sku":sku}
    return collection.delete_one(query).acknowledged

def read_chat():
    result = list()
    for i in collection_chat.find():
        result.append(dict(
            name=i['name'],
            chat=i['chat'],
            status=i['status']
        ))
    return result



# print(delete_doc_by_sku(sku="B004-0116"))
# docs = collection_chat.find()
#
# for d in docs:
#     print(d)

# print(find_chat_by_name("Farhan Muhammad"))

# print(collection_chat.delete_one({"name":"Farhan Muhammad"}).acknowledged)

from selenium import webdriver
import os
import _constants
from update import update_stok_by_tokped_id
import time
from inputreader import read_tsv, read_csv
import requests
import pprint
import csv
import re
from mongo import insert_doc, find_by_sku, update_doc
from authentication import authetication

def download_pic(image_url):
    try:
        with open("pictures/product.jpg", 'wb') as handle:
            response = requests.get(image_url, stream=True)

            if not response.ok:
                return response.status_code

            for block in response.iter_content(1024):
                if not block:
                    break
                handle.write(block)
            print("pic downloaded")
            return True
    except:
        return False

def create_product(driver,
                   product_name,
                   image_url,
                   price,
                   stock,
                   description,
                   sku,
                   weight,
                   category,
                   etalase,
                   code_product=None,
                   preorder_day=None,
                   preorder=False,
                   must_insurance=False,
                   headless=False):

    def switch_category(category):
        category = str(category).lower().replace(" ", "_")
        cat = dict(
            kunci_l="a[rel='number:1061']",
            tool_box="a[rel='number:1064']",
            tang="a[rel='number:1059']",
            kunci_inggris="a[rel='number:1060']",
            obeng="a[rel='number:1057']",
            lainnya="a[rel='number:1065']",
            bor="a[rel='number:1062']",
            meteran="a[rel='number:1073']"
        )

        return cat.get(category, None)

    def switch_etalase(etalase):
        category = str(etalase).lower().replace(" ", "_")
        print("etalase: %s" % category)
        cat = dict(
            pliers="a[rel='number:9716067']",
            sockets="a[rel='number:8170716']",
            surveying="a[rel='number:9716065']",
            hand_tools="a[rel='number:17672207']",
            wrench="a[rel='number:8170717']",
            power_tools="a[rel='number:17732588']",
            screwdrivers="a[rel='number:9716079']"
        )

        return cat.get(category, "a[rel='number:8170717']")

    def insert(product_name=product_name):
        try:
            print("creating product for %s, please wait.." % product_name)
            print("downloading image..")
            print("img_url: %s" % image_url)
            d = download_pic(image_url)
            while not d:
                d = download_pic(image_url)

            if str(d) == "404":
                return "sku %s error, image url was not found.."

            def click_component(i):
                try:
                    i.click()
                except:
                    return False



            driver.get("https://www.tokopedia.com/product-add.pl?nref=addpdside")

            print("uploading image..")
            # upload images
            image_path = os.path.join(os.getcwd(), "pictures", "product.jpg")
            js = "document.getElementById('ngf-pickfiles-nav1').style.visibility = 'visible';"
            driver.execute_script(js)
            field = driver.find_element_by_id("ngf-pickfiles-nav1")
            field.send_keys(image_path)
            driver.implicitly_wait(1)

            print("Creating product name..")
            # product name
            def insert_product_name(product_name):
                if code_product:
                    product_name += " %s" % code_product
                product_name_field = driver.find_element_by_id("p-name")
                product_name_field.send_keys(product_name)
                driver.implicitly_wait(1)

                inserted = product_name_field.get_attribute("value")
                if inserted and len(str(inserted)) > 1:
                    return True
                else:
                    return False

            while not insert_product_name(product_name=product_name):
                insert_product_name(product_name=product_name)

            print("Selecting categori 1")
            # kategori 1
            driver.find_element_by_class_name("selectBox-label").click()
            opt = driver.find_elements_by_css_selector(".opt")
            for i in opt:
                if str(i.text).strip() == "Rumah Tangga":
                    while click_component(i) == False:
                        click_component(i)
                    break
            time.sleep(2)

            print("Selecting categori 2..")
            # kategori 2
            select_box = driver.find_elements_by_class_name("selectBox-label")
            for i in range(len(select_box)):
                while click_component(select_box[1]) == False:
                    click_component(select_box[1])
                break
            i = driver.find_element_by_css_selector("a[rel='number:994']")
            while click_component(i) == False:
                click_component(i)
            driver.implicitly_wait(1)
            time.sleep(2)
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

            print("Selecting categori 3: %s" % category)
            # kategori 3
            select_box = driver.find_elements_by_class_name("selectBox-label")
            for i in range(len(select_box)):
                while click_component(select_box[2]) == False:
                    click_component(select_box[2])
                break
            i = driver.find_element_by_css_selector(switch_category(category))
            while click_component(i) == False:
                click_component(i)
            time.sleep(2)
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

            print("Selecting etalase..")
            # pilih etalase
            box_label = driver.find_elements_by_class_name("selectBox-label")
            for i in box_label:
                if str(i.text).strip() == "Pilih Etalase":
                    while click_component(i) == False:
                        click_component(i)
                    break

            i = driver.find_element_by_css_selector(switch_etalase(etalase))
            while click_component(i) == False:
                click_component(i)
            driver.implicitly_wait(1)

            print("Inserting price field..")
            # insert price field
            def insert_price():
                price_field = driver.find_element_by_id("p-price")
                price_field.send_keys(price)
                driver.implicitly_wait(1)

                value = price_field.get_attribute("value")
                if value and len(str(value)) > 0:
                    return True
                else:
                    return False

            while not insert_price():
                insert_price()

            print("Inserting stok field..")
            # insert stock field
            def insert_stock():
                stock_field = driver.find_element_by_id("invenage-value")
                stock_field.send_keys(stock)
                driver.implicitly_wait(1)

                value = stock_field.get_attribute("value")
                if value and len(str(value)) > 0:
                    return True
                else:
                    return False

            while not insert_stock():
                insert_stock()

            print("Inserting sku field..")
            # insert sku field
            def insert_sku():
                sku_field = driver.find_element_by_id("p-sku")
                sku_field.send_keys(sku)
                driver.implicitly_wait(1)

                value = sku_field.get_attribute("value")
                if value and len(str(value)) > 0:
                    return True
                else:
                    return False

            while not insert_sku():
                insert_sku()

            print("Inserting description..")
            # insert description
            def insert_description():
                description_field = driver.find_element_by_id("p-description")
                description_field.send_keys(description)
                driver.implicitly_wait(1)

                value = description_field.get_attribute("value")
                if value and len(str(value)) > 0:
                    return True
                else:
                    return False

            while not insert_description():
                insert_description()


            def insert_weight():
                weight_filed = driver.find_element_by_id("p-weight")
                weight_filed.send_keys(weight)
                driver.implicitly_wait(1)

                value = weight_filed.get_attribute("value")

                if value and len(str(value)) > 0:
                    return True
                else:
                    return False

            print("Inserting weight %s" % weight)
            # inserting weight
            while not insert_weight():
                print("reinserting weight..")
                insert_weight()
            # weight_filed = driver.find_element_by_id("p-weight")
            # weight_filed.send_keys(weight)
            # driver.implicitly_wait(1)

            # value = weight_filed.get_attribute("value")

            # insert must insurance
            if must_insurance:
                insurance_radio = driver.find_element_by_id("must_insurance")
                while click_component(insurance_radio) == False:
                    click_component(insurance_radio)

            # insert preorder
            if preorder:
                print("Inserting preorder..")
                preoder_radio = driver.find_element_by_css_selector("div.switch_preorder div.toggle-track")
                while click_component(preoder_radio) == False:
                    click_component(preoder_radio)

                preorder_day_field = driver.find_element_by_id("po-process-value")
                preorder_day_field.send_keys(preorder_day)
                driver.implicitly_wait(1)

            # click save
            print("clicking save..")
            save_product = driver.find_element_by_id("s-save-prod")
            trying = 0
            while click_component(save_product) == False:
                if trying > 3:
                    break
                print("reclicking..")
                click_component(save_product)
                trying += 1

            time.sleep(5)
            print("%s was created.." % product_name)

            driver.get("https://www.tokopedia.com/manage-product-new.pl?nref=pdlstside&edited=1")
            tokped_id = driver.find_element_by_class_name("copy-product").get_attribute("href")
            tokped_id = tokped_id[tokped_id.find('?'):][4:]
            return dict(
                product_name=product_name,
                image_url=image_url,
                price=price,
                stock=stock,
                description=description,
                sku=sku,
                weight=weight,
                category=category,
                etalase=etalase,
                tokped_id=tokped_id,
            )
        except Exception as e:
            print("insert product error: %s" % str(e))
            return None

    result = insert()
    return result

def insert_bulk(inputfile:list, headless=False):

    total_products = len(inputfile)
    inserted = 0
    created_products = list()

    for product in inputfile:
        sku = find_by_sku(product['id'])
        if not sku:
            driver = authetication(headless=headless)
            item = create_product(driver=driver,
                           product_name=product['name'],
                           image_url=product['image_1'],
                           price=str(int(float(product['price']))),
                           stock=product['inventory_qty'] if product['inventory_qty'] is not '' else 1,
                           description=product['description_tokopedia'],
                           sku=product['id'],
                           weight=int(float(product['weight'])) if int(float(product['weight'])) is not 0 else 1,
                           category=product['category'],
                           etalase=product['etalase'])
            inserted += 1
            if "image url was not found.." in item:
                print("sku: %s image url was not found.." % product['id'])
                driver.quit()
            else:
                created_products.append(item)
                print("item: %s" % item)
                print(insert_doc(doc=item))
                print("%s/%s products has been created.." % (inserted, total_products))
                driver.quit()
        else:
            print("sku %s is already created.." % sku['sku'])
            # try:
            #     driver = authetication(headless=headless)
            #     try:
            #         tokped_id = sku['tokped_id']
            #     except:
            #         tokped_id = sku['id']
            #     print("sku %s is already created.." % sku['sku'])
            #     update_stok_by_tokped_id(driver=driver,
            #                              tokped_id=tokped_id,
            #                              image_url=sku.get('image_url', None),
            #                              description=sku.get("description", None),
            #                              price=sku.get("price", None),
            #                              new_name=sku.get("produc_name", None))
            #     print(update_doc(new_doc=product, old_doc={"sku": sku}))
            #     driver.quit()
            # except:
            #     pass
            pass

    print("done..")
    # keys = created_products[0].keys()
    # with open('created_products.csv', 'w') as file:
    #     dict_writer = csv.DictWriter(file, keys)
    #     dict_writer.writeheader()
    #     dict_writer.writerows(created_products)


if __name__ == '__main__':
    inputfile = read_csv("products2.csv")
    # product = read_csv("products.csv")[0]
    # driver = authetication()
    inputfile = [i for i in inputfile if 'Bosch Velcro Sanding Disc 125mm P60 / G60' in i['name']]
    insert_bulk(inputfile, headless=False)
    # result = create_product(driver=driver,
    #                    product_name=product['name'],
    #                    image_url=product['image_1'],
    #                    price=str(int(float(product['price']))),
    #                    stock=product['inventory_qty'] if product['inventory_qty'] is not '' else 1,
    #                    description=product['description_tokopedia'],
    #                    sku=product['id'],
    #                    weight=int(float(product['weight'])) if int(float(product['weight'])) is not 0 else 1,
    #                    category=product['category'],
    #                    etalase=product['etalase'])
    # print(result)
    # name = [i.get('name') for i in inputfile]
    # for n in name:
    #     print(n)



    # create_product(product_name="Sepatu Biasa Jangan Dibeli", code_product="2d34g", pic_name="sepatu.jpg", price=30000,stock=200, description="jual sepatu biasa aja", sku="45hhc", weight=1)
    # pprint.pprint(inputfile)
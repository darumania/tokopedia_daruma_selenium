from selenium import webdriver
import os
import datetime
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pyautogui
import time

start_time = datetime.datetime.now()


def click_component(i):
    try:
        i.click()
    except:
        return False

options = webdriver.FirefoxOptions()
# options.add_argument('-headless')
driver = webdriver.Firefox(firefox_options=options, executable_path=os.path.join(os.getcwd(), "geckodriver"))
driver.get("https://www.tokopedia.com/login")
email_field = driver.find_element_by_name("email")
password_field = driver.find_element_by_name("password")

email_field.send_keys("surel.farhan@gmail.com")
driver.implicitly_wait(1)

password_field.send_keys("Smansa11")
driver.implicitly_wait(1)

driver.find_element_by_class_name("js__submit-login").click()

driver.get("https://www.tokopedia.com/product-add.pl?nref=addpdside")

def upload_file(i, file_path):
    try:
        i.send_key(file_path)
    except:
        return False


image_path = os.path.join(os.getcwd(), "pictures", "vans.jpg")
# driver.execute_script("""x`x
#     document.addEventListener('click', function(evt) {
#       if (evt.target.type === 'file')
#         evt.preventDefault();
#     }, true)
#     """)
image_field = driver.find_element_by_id("pickfiles-nav1")
driver.execute_script("arguments[0].setAttribute('ngf-select','add_files(%s)')" % image_path, image_field)

# driver.find_element_by_id("pickfiles-nav1").click()
# driver.execute_script("arguments[0].removeAttribute('class');", image_field)
# while upload_file(image_field, image_path) == False:
#     upload_file(image_field, image_path)
# image_field = driver.find_element_by_id("pickfiles-nav1")
# image_field = driver.find_element_by_id("pickfile-container")
# image_field.send_keys(image_path)

driver.implicitly_wait(1)
# driver.find_element_by_id("pickfiles-nav1").click()
# pyautogui.moveTo(212, 341)
# pyautogui.click()
# pyautogui.moveTo(454, 147)
# pyautogui.click(clicks=2, interval=0.25)
# pyautogui.moveTo(454, 148)
# pyautogui.click(interval=0.25)
# # pyautogui.press('enter')
# pyautogui.moveTo(454, 148)
# pyautogui.click(clicks=2, interval=0.25)

product_name = driver.find_element_by_id("p-name")
product_name.send_keys("sepatu biasa aja")
driver.implicitly_wait(1)

driver.find_element_by_class_name("selectBox-label").click()
opt = driver.find_elements_by_css_selector(".opt")
for i in opt:
    if str(i.text).strip() == "Fashion Wanita":
        i.click()
        break
time.sleep(4)

select_box = driver.find_elements_by_class_name("selectBox-label")
for i in range(len(select_box)):
    while click_component(select_box[1]) == False:
        click_component(select_box[1])
    break

i = driver.find_element_by_css_selector("a[rel='number:1762']")
while click_component(i) == False:
    click_component(i)

time.sleep(4)
# driver.implicitly_wait(3)

select_box = driver.find_elements_by_class_name("selectBox-label")
for i in range(len(select_box)):
    while click_component(select_box[2]) == False:
        click_component(select_box[2])
    break
time.sleep(2)

i = driver.find_element_by_css_selector("a[rel='number:1766']")
while click_component(i) == False:
    click_component(i)
# driver.implicitly_wait(3)

# select_box = driver.find_elements_by_class_name("selectBox-arrow")
# for i in select_box:
#     print(i.text)



box_label = driver.find_elements_by_class_name("selectBox-label")
for i in box_label:
    if str(i.text).strip() == "Pilih Etalase":
        while click_component(i) == False:
            click_component(i)
        break

time.sleep(2)
i = driver.find_element_by_css_selector("a[rel='number:17125121']")
while click_component(i) == False:
    click_component(i)


# driver.find_element_by_css_selector("a[rel='number:17125121']").click()
# select_box = driver.find_elements_by_class_name("selectBox-label")
# for i in select_box:
#     print(i.text)
    # if str(i.text).strip() == "Celana":
    #     i.click()
    #     break
driver.implicitly_wait(1)

price_field = driver.find_element_by_id("p-price")
price_field.send_keys("200000")
driver.implicitly_wait(1)

stock_field = driver.find_element_by_id("invenage-value")
stock_field.send_keys("35")
driver.implicitly_wait(1)

sku_field = driver.find_element_by_id("p-sku")
sku_field.send_keys("kkkjcns")
driver.implicitly_wait(1)

description_field = driver.find_element_by_id("p-description")
description_field.send_keys("Jual sepatu yang biasa aja. tolong dibeli yaaa")
driver.implicitly_wait(1)

weight_filed = driver.find_element_by_id("p-weight")
weight_filed.send_keys("1")
driver.implicitly_wait(1)

insurance_radio = driver.find_element_by_id("must_insurance")
while click_component(insurance_radio) == False:
    click_component(insurance_radio)

preoder = driver.find_element_by_css_selector("div.switch_preorder div.toggle-track")
while click_component(preoder) == False:
    click_component(preoder)

preorder_day = driver.find_element_by_id("po-process-value")
preorder_day.send_keys("5")
driver.implicitly_wait(1)

save_product = driver.find_element_by_id("s-save-prod")
while click_component(save_product) == False:
    click_component(save_product)

# driver.find_element_by_xpath('//a[@rel="number:1775"]').click()


end_time = datetime.datetime.now()
result = end_time - start_time
print(divmod(result.days * 86400 + result.seconds, 60))